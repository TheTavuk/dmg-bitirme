import json
from my_model import create_model, CLASS_MAP, MinMaxNormalize
import tensorflow as tf
import numpy as np
from datetime import date, datetime

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render


def predict(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        content = body['points']
        data =[]
        
        for i in content:
            temp = []
            temp.append(i['x'])
            temp.append(i['z'])
            temp.append(i['y'])
            data.append(temp)    
        f = open(f'scans/{datetime.now().strftime("%d-%m-%Y-%H-%M-%S")}.txt', 'x')
        f.write(str(data)) 
        c = []
        c.append(data)
        arr = np.array(c)

        arr[0] = MinMaxNormalize(arr[0])

        m = create_model(num_of_points=len(arr[0]), load_weights=True)
        d = m.predict(arr)
        
        key_list = list(CLASS_MAP.values())
        value_list = d[0][:7]
        value_list /= sum(value_list)
        value_list *= 100
        zip_iterator = zip(key_list, list(value_list.astype(float)))
        prob_dict = dict(zip_iterator)
        p = max(d[0])
        i = np.where(d[0] == p)
        data = {'pred': CLASS_MAP[i[0][0]]}
        data += prob_dict
        return JsonResponse(data=data,)



    else:
        return HttpResponse('text')
